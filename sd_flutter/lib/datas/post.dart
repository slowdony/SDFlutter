 // ignore_for_file: prefer_const_constructors

 class Post {
   const Post({
     required this.title,
     required this.name,
     required this.imageurl,
   });
   //final 修饰的变量一旦赋值后就不可修改
   final String title;
   final String name;
   final String imageurl;
 }
 
 //数据源
 final List<Post> posts = [
   Post(
     title: '盘点国内那些免费好用的图床',
     name: "s",
     imageurl: "https://pic.downk.cc/item/5e7b64fd504f4bcb040fae8f.jpg",
   ),
   Post(
     title: "春水初生，春林初盛，春风十里，不如你。",
     name: "s",
     imageurl: "http://gank.io/images/cfb4028bfead41e8b6e34057364969d1",
   ),
   Post(
     title: "盘点国内那些免费好用的图床",
     name: "s",
     imageurl: "https://pic.downk.cc/item/5e7b64fd504f4bcb040fae8f.jpg",
   ),
   Post(
     title: "春水初生，春林初盛，春风十里，不如你。",
     name: "s",
     imageurl: "http://gank.io/images/cfb4028bfead41e8b6e34057364969d1",
   ),
    Post(
     title: "干货集中营新版更新",
     name: "s",
     imageurl: "https://pic.downk.cc/item/5e7b64fd504f4bcb040fae8f.jpg",
   ),
    Post(
     title: "盘点国内那些免费好用的图床",
     name: "s",
     imageurl: "http://gank.io/images/cfb4028bfead41e8b6e34057364969d1",
   ),
    Post(
     title: "干货集中营新版更新",
     name: "s",
     imageurl: "http://gank.io/images/aebca647b3054757afd0e54d83e0628e",
   ),
    Post(
     title: "春水初生，春林初盛，春风十里，不如你。",
     name: "s",
     imageurl: "http://gank.io/images/cfb4028bfead41e8b6e34057364969d1",
   )
 ];