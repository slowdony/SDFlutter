// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

//data

import './datas/post.dart';

//ui
import './demo/hello_demo.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ListDemo();
}
}

// ignore: use_key_in_widget_constructors
class ListDemo extends StatelessWidget {
  Widget _listcell (BuildContext context,int index){
    return Container(
      color:Colors.white,
      margin: EdgeInsets.all(8.0),
      child: Column(
        children: <Widget>[
          Image.network(posts[index].imageurl),
          SizedBox(height: 16.0),
          Text(
            posts[index].name,
            style: Theme.of(context).textTheme.bodyText1,
          ),
          Text(
            posts[index].title,
            style:Theme.of(context).textTheme.bodyText2,
          ),
          SizedBox(height: 8),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('导航栏'),
          elevation: 0,
        ),
        body: ListView.builder(
          itemCount: posts.length,
          itemBuilder: _listcell,
          )
      ),
      theme: ThemeData(
        //导航栏颜色
        primarySwatch: Colors.yellow
      ),
    );
  }
}

