// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

//hello 组件
// ignore: use_key_in_widget_constructors
class Hello extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        'hello demo ',
        textDirection: TextDirection.ltr,
        style: TextStyle(
          fontSize: 32,
          fontWeight: FontWeight.bold,
          color:Colors.blue
        )),
    );
  }
}